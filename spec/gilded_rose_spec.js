describe("Gilded Rose", function () {

  it("should lower Quality and Sellin values", function () {
    items = [new Item('+5 Dexterity Vest', 10, 20)];
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].sell_in).toEqual(7);
    expect(items[0].quality).toEqual(17);
  });

  it("should lower Quality twice once the sell by date has passed", function () {
    items = [new Item('+5 Dexterity Vest', 5, 20)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(10);
  });

  it("should never have negative value for Quality", function () {
    items = [new Item('+5 Dexterity Vest', 5, 5)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(0);
  });

  it("should increase Quality for Aged Brie when it gets older", function () {
    items = [new Item('Aged Brie', 5, 5)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(12);
  });

  it("should never have Quality bigger than 50", function () {
    items = [new Item('Aged Brie', 15, 49)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(50);
  });

  it("should never decrease Quality for Sulfuras", function () {
    items = [new Item('Sulfuras, Hand of Ragnaros', 15, 49)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(49);
  });

  it("should increase Quality by 2 for Backstage passes when there are 10 days or less left", function () {
    items = [new Item('Backstage passes to a TAFKAL80ETC concert', 11, 49)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(56);
  });

  it("should increase Quality by 3 for Backstage passes when there are 5 days or less left", function () {
    items = [new Item('Backstage passes to a TAFKAL80ETC concert', 6, 49)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(60);
  });

  it("should drop Quality to 0 for Backstage passes after the concert", function () {
    items = [new Item('Backstage passes to a TAFKAL80ETC concert', 3, 49)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(0);
  });

  it("should decrease Quality twice faster for Conjured", function () {
    items = [new Item('Conjured Mana Cake', 3, 49)];
    update_quality();
    update_quality();
    update_quality();
    update_quality();
    expect(items[0].quality).toEqual(41);
  });

});
