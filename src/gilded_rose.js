function Item(name, sell_in, quality) {
  this.name = name;
  this.sell_in = sell_in;
  this.quality = quality;
}

let items = []

function update_quality() {
  for (let i = 0; i < items.length; i++) {
    let initialSellIn = items[i].sell_in;
    let initialQuality = items[i].quality;

    // At the end of each day our system lowers both values for every item.
    if (items[i].sell_in > 0) {
      items[i].sell_in--;
    }
    if (items[i].quality > 0) {
      items[i].quality--;
    }

    // Once the sell by date has passed, Quality degrades twice as fast
    if (items[i].sell_in <= 0) {
      items[i].quality--;
    }

    // The Quality of an item is never negative.
    if (items[i].quality < 0) {
      items[i].quality = 0;
    }

    // "Aged Brie" actually increases in Quality the older it gets.
    if (items[i].name === 'Aged Brie') {
      items[i].quality = initialQuality + 1;
    }

    // The Quality of an item is never more than 50.
    if (items[i].quality > 50) {
      items[i].quality = 50;
    }

    // "Sulfuras", being a legendary item, never has to be sold or decreases in Quality.
    if (items[i].name === 'Sulfuras, Hand of Ragnaros') {
      items[i].sell_in = initialSellIn;
      items[i].quality = initialQuality;
    }

    // "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches,
    // Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
    // Quality drops to 0 after the concert.
    if (items[i].name === 'Backstage passes to a TAFKAL80ETC concert') {
      if (initialSellIn > 10) {
        items[i].quality = initialQuality + 1;
      } else if (initialSellIn > 5 && initialSellIn <= 10) {
        items[i].quality = initialQuality + 2;
      } else if (initialSellIn > 0 && initialSellIn <= 5) {
        items[i].quality = initialQuality + 3;
      } else if (initialSellIn === 0) {
        items[i].quality = 0;
      }
    }

    // "Conjured" items degrade in Quality twice as fast as normal items.
    if (items[i].name === 'Conjured Mana Cake') {
      if (initialQuality > 0) {
        items[i].quality = initialQuality - 2;
      }
    }

  }
}
